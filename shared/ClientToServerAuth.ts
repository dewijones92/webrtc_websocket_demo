
/**
 * Dewi was here  ddd
 */
type EAuthType = "LOGIN" | "END" | "ICE_CANDIDATE" | "Offer" | "Answer"

export interface ILogin {
    value: IName
}

export interface IName {
    value: string
}

export interface I_ICE_CANDIDATE {
    ice_candidate: RTCIceCandidate,
    dest: IName
}

export interface I_OFFER {
    offer: RTCSessionDescriptionInit,
    /**
     * Who originally made the call
     */
    source: IName,
    dest: IName
}

export type I_Answer = {
    answer: RTCSessionDescriptionInit,
    /**
     * Who originally made the call
     */
    source: IName,
    dest: IName
}



export interface DTO{
    type: EAuthType;

    /**
     * IAuth etc etc
     */
    data: ILogin | IName | I_ICE_CANDIDATE | I_OFFER | I_Answer;
}
