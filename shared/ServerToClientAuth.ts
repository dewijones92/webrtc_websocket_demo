import { IName } from "./ClientToServerAuth";

/**
 * Dewi was here  ddd
 */
type EDataType = "ERROR" | "SUCCESS" | "OFFER" | "ANSWER" | "I_ICE_Candidate" | "IUserOnline"

export interface IError {
    message: string
}
/**
 * Success login
 */
export interface ISuccess {
    message: string,
    my_name: IName
}

export interface IOffer {
    offer: RTCSessionDescriptionInit,
    source: IName,
    dest: IName
}

export interface IAnswer {
    answer: RTCSessionDescriptionInit
}

export interface IUserOnline {
    user_string_formatted: string
}

export interface I_ICE_CANDIDATE {
    ice_candidate: RTCIceCandidate
}

export interface DTO {
    type: EDataType;

    /**
     * IError etc etc
     */
    data: IError | ISuccess | IOffer | I_ICE_CANDIDATE | IUserOnline | IAnswer ;
}



