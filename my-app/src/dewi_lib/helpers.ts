import * as Shared from "../shared/Shared"
import * as ClientToServerAuth from "../shared/ClientToServerAuth"
import * as ServerToClientAuth from "../shared/ServerToClientAuth"
import { off } from "process";


export const CLIENT_TAG = "CLIENT_TAG"

var myName: ClientToServerAuth.IName
var remoteUser: ClientToServerAuth.IName
var myConnection: RTCPeerConnection
var dataChannel: RTCDataChannel;

export const addtrack = (track: MediaStreamTrack, localStream: MediaStream) => {
    log(`adding track to stream myConnection`)
    myConnection.addTrack(track, localStream);
}

const socket = new WebSocket(`ws://192.168.1.11:${Shared.SERVER_PORT}`);

socket.onmessage = (ev: MessageEvent) => {
    const message = JSON.parse(ev.data) as ServerToClientAuth.DTO;

    log(`RECEIVING onmessage ${ev.data}`);


    switch(message.type){
        case "ERROR": {
            const data = message.data as ServerToClientAuth.IError;
            break;
        }
        case "SUCCESS": {
            const data = message.data as ServerToClientAuth.ISuccess;
            onLogin(data);
            break;
        }
        case "OFFER": {
            const data = message.data as ServerToClientAuth.IOffer;
            onOffer(data);
            break;
        }
        case "ANSWER": {
            const data = message.data as ServerToClientAuth.IAnswer;
            onAnswer(data);
            break;
        }
        case "I_ICE_Candidate": {
            const data = message.data as ServerToClientAuth.I_ICE_CANDIDATE;
            onCandidate(data.ice_candidate);
            break;
        }
        case "IUserOnline": {
            const data = message.data as ServerToClientAuth.IUserOnline;
            log(JSON.stringify(data));
            break;
        }
    }
}

socket.onerror = (ev: Event) => console.log(`${CLIENT_TAG} onerror: ${ev}`);
socket.onopen = (ev: Event) => console.log(`${CLIENT_TAG} onopen: ${ev}`)


export type LogUpdate = (n: string) => void;

var consoleupdate:LogUpdate;
var onLoginUpdate: () => void;

export const setOnConsoleUpdate = (callback: LogUpdate)  => {
    consoleupdate = callback;
};
export const setOnLoginUpdate = async (callback: () => void) => onLoginUpdate = callback;

export const login = (name: ClientToServerAuth.IName) =>{
    const loginDeets: ClientToServerAuth.ILogin = {value: name};

    send({type: "LOGIN", data: loginDeets})
} 

type StreamUpdate = (stream: MediaStream) => void
var streamAddListender: StreamUpdate;

export const setOnAddStreamUpdate = async (callback: StreamUpdate) => streamAddListender = callback;

const onLogin = (data: ServerToClientAuth.ISuccess) => {
   
    myName = data.my_name;
    
    myConnection = new RTCPeerConnection({
        iceServers: [
          {
            urls: "stun:stun.1.google.com:19302"
          }
        ]
      });

    myConnection.onicecandidateerror = (error: RTCPeerConnectionIceErrorEvent) => {
        log(`onicecandidateerror ${JSON.stringify(error)}`)
    }

    log(`ice connection is created`);

    myConnection.ontrack = (track: RTCTrackEvent) => {
        console.log(`myConnection  ontrack`)
        streamAddListender(track.streams[0]);
    }

    
    //setup ice handling 
    //when the browser finds an ice candidate we send it to another peer
    myConnection.onicecandidate = (event: RTCPeerConnectionIceEvent) => {
        log(`onicecandidate_onicecandidate`);
        if(event.candidate != null){
            log(`onicecandidate_remoteUser: ${JSON.stringify(remoteUser)}`);
            const iceSend: ClientToServerAuth.I_ICE_CANDIDATE = {ice_candidate: event.candidate, dest: remoteUser }
            send({type: "ICE_CANDIDATE", data: iceSend });
        }
    }

    onLoginUpdate();
}

/**
 * When someone wants to call us
 * @param offer 
 */
const onOffer = async (offer: ServerToClientAuth.IOffer) => {
    remoteUser = offer.source;
    myConnection.setRemoteDescription(new RTCSessionDescription(offer.offer));

    const answer = await myConnection.createAnswer();
    myConnection.setLocalDescription(answer);

    const data_to_send: ClientToServerAuth.I_Answer = {
        answer: answer,
        source: remoteUser,
        dest: myName
    }

    send({type: "Answer", data: data_to_send});

}

/**
 * setup a peer connection with another user 
 * @param caller 
 */
export const call = async (person_to_call: ClientToServerAuth.IName): Promise<void> => {
    log(`gonna call ${person_to_call.value}`)
    remoteUser = person_to_call;
    const offer = await myConnection.createOffer();
    
    const send_data: ClientToServerAuth.I_OFFER = {offer: offer, source: myName, dest: person_to_call};
    
    send({type: "Offer", data: send_data});
    myConnection.setLocalDescription(offer);
}


/**
 * when another user answers to our offer 
 * @param offer 
 * @param name 
 */
const onAnswer = async (answer: ServerToClientAuth.IAnswer) => {
    myConnection.setRemoteDescription(new RTCSessionDescription(answer.answer))
}

/**
 * when we got ice candidate from another user 
 * @param candidate 
 */
const onCandidate = async (candidate: RTCIceCandidate) => {
    log(`adding ice candidate: ${JSON.stringify(candidate)}`);
    await myConnection.addIceCandidate(candidate);
}

/**
 * create channel
 */
const openDataChannel = async () => {
    
   var dataChannelOptions = { 
    reliable:true 
   }; 

    dataChannel = myConnection.createDataChannel("myDataChannel", dataChannelOptions as any);

    dataChannel.onerror = (error: RTCErrorEvent) => {
        log(`dataChannel Error: ${error}`)
    }

    dataChannel.onmessage = (event: Event) => {
        log(`datachannel onMessage ${JSON.stringify(event)}`)
    }
}

const sendMessage = async (message: string) => {
    log(`sendMessage: ${message}`);
    dataChannel.send(message);
}



export const log = (value: string) => {
    const message = `11CLIENT_TAG: ${JSON.stringify(value)}`;
    consoleupdate(message);
    console.log(message)
};

const send = (object: ClientToServerAuth.DTO) => {
    let data = JSON.stringify(object);

    log(`SENDING ${data}`);
    socket.send(data)
};


