import React, { useState, VideoHTMLAttributes, useEffect, useRef } from 'react';
import './App.css';
import * as Helpers from "./dewi_lib/helpers"
import { CSSProperties } from "react";
import { IName } from './shared/ClientToServerAuth';




interface IState {
  console_log: string,
  caller: IName,
  me_video_src: MediaStream,
  peer_video_src: MediaStream
}


type IPropsType = {}


class App extends React.Component<IPropsType, IState> {
  divStyle: CSSProperties = {
    whiteSpace: 'pre-wrap',
  }

  rand = Math.random()*100;

  login = async () => {
    alert("asdasd");

    Helpers.setOnConsoleUpdate((log) => {
      this.setState({ console_log: this.state.console_log + "\n" + log })
    });

    Helpers.setOnAddStreamUpdate((stream: MediaStream) => {
      this.setState({ peer_video_src: stream})
    })

    Helpers.setOnLoginUpdate(async () => {

      Helpers.log(`this.constraints; ${JSON.stringify(this.constraints)}`);
      //enabling video and audio channels 
      navigator.getUserMedia(this.constraints as any, (stream) => {

        Helpers.log(`getUserMedia stream: ${stream}`);
        this.setState({ me_video_src: stream as any })

        stream.getTracks().forEach(track => {
          Helpers.addtrack(track, stream);
        });


      },  (err) => { Helpers.log(`error getUserMedia: ${err}`) });



    });

    Helpers.login({ value: `DEWI${this.rand}` });


  }

  constructor(props: IPropsType) {
    super(props);

    this.state = {
      console_log: "",
      caller: {value: ""},
      me_video_src: null as any,
      peer_video_src: null as any
    }
  }

  //constraints for desktop browser 
  desktopConstraints = {

    video: true,

    audio: true
  }; 

  constraints = this.desktopConstraints; 


  render() {
    return (
      <div className="App">
        <header className="App-header">
          <div>
            ME
          {this.state.me_video_src != null &&
              <video autoPlay={true} ref={audio => { if (audio != null) audio.srcObject = this.state.me_video_src }} ></video>
            }
          </div>
          <br></br>
          <div>
            PEER
          {this.state.peer_video_src != null &&
              <video autoPlay={true} ref={audio => { if (audio != null) audio.srcObject = this.state.peer_video_src }} ></video>
            }
          </div>
          <button onClick={this.login}>login</button>
          <div>
            who you wanna call??
            <input value={this.state.caller.value} onChange={evt => this.updateInputValue(evt)} />
            <button onClick={() => Helpers.call(this.state.caller)}>CALL</button>
          </div>
          <div style={this.divStyle}>{this.state.console_log}</div>
        </header>
      </div>
    );
  }
  updateInputValue(evt: any) {
    const callerValue  = evt.target.value;
    const callerObject: IName = {value: callerValue};
    console.log(`callerObject: ${JSON.stringify(callerObject)}`);
    this.setState({
      caller: callerObject
    });
  }
}

export default App;
