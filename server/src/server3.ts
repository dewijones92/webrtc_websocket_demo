import * as http from 'http';
import * as ClientToServerAuth from "./shared/ClientToServerAuth"
import * as ServerToClientAuth from "./shared/ServerToClientAuth"

import * as Shared from "./shared/Shared"

import * as WebSocket from 'ws';




class DewiServer3 {
    private static TAG = `DEWI_SERVER_TAG`;


    private userManager = new (class UserManager {
        /**
         * Who is connected with who
         */
        private connectedUsers: { [name: string]: string } = {};

        private users: { [name: string]: WebSocket } = {};
        addUser = (name: ClientToServerAuth.IName, socket: WebSocket) => {
            console.log(`addUser: ${name.value}`)
            this.users[name.value] = socket;
        }
        removeUser = (name: ClientToServerAuth.IName) => delete this.users[name.value]
        removeUserBySocket = (socket: WebSocket) =>  {
            const user = this.getUserBySocket(socket);
            console.log(`trying to delete user by socket`);
            console.log(`user is ${user != null ? `NOT` :`IS`} null`);
            if(user == null) return;
            delete this.users[user.value];
        }
        getUserBySocket = (socket: WebSocket): ClientToServerAuth.IName => {
            for (let [key, value] of Object.entries(this.users)) {
                if (value == socket) {
                    return { value: key };
                }
            }
        }
        getUser = (name: ClientToServerAuth.IName) => this.users[name.value]

        getUsers = (): [ClientToServerAuth.IName, WebSocket][] => {
            var users: [ClientToServerAuth.IName, WebSocket][] = [];

            for (let [key, value] of Object.entries(this.users)) {
                console.log(`getusers adding ${key}`)
                users.push([{value: key}, value])
            }
            return users;

        };

        /**
         * Who is connected with who
         * @param name 
         * @param pairedWith 
         */
        addConnectedUser = (name: ClientToServerAuth.IName, pairedWith: ClientToServerAuth.IName) => {
            console.log(`addConnectedUser: pesonA: ${name.value} . personB: ${pairedWith.value}`)
            this.connectedUsers[name.value] = pairedWith.value
        }
        getConnectedUser = (name: ClientToServerAuth.IName) => this.connectedUsers[name.value]
        removeConnectedUser = (name: ClientToServerAuth.IName) => delete this.connectedUsers[name.value]
    })();

    constructor() {


        let wss = new WebSocket.Server({ port: Shared.SERVER_PORT });
        console.log(`${DewiServer3.TAG} listening on port num ${Shared.SERVER_PORT}`);


        wss.on(`connection`, (client_socket: WebSocket, request: http.IncomingMessage) => {
            console.log(`${DewiServer3.TAG} user connected`)


            client_socket.onmessage = (message: WebSocket.MessageEvent) => {
                const data = message.data.valueOf() as string;
                console.log(`RECEIVE MESSAGE ${DewiServer3.TAG}  received: %s`, data);


                var requestAuth: ClientToServerAuth.DTO;

                //accepting only JSON messages 
              
                    requestAuth = JSON.parse(data);

                    console.log(`message data: ${JSON.stringify(requestAuth)}`);

                    switch (requestAuth.type) {
                        case "LOGIN": {
                            const data = requestAuth.data as ClientToServerAuth.ILogin;


                            this.userManager.addUser({ value: data.value.value }, client_socket);

                      

                            const sendMessage: ServerToClientAuth.ISuccess = {message: "Success login", my_name: data.value };

                            this.send_to_client({type: "SUCCESS", data: sendMessage}, client_socket);

                            printClients();
                            break;
                        }
                        case "ICE_CANDIDATE": {
                            //from userA
                            const data = requestAuth.data as ClientToServerAuth.I_ICE_CANDIDATE;
                            data.ice_candidate;

                            const userBConn = this.userManager.getUser(data.dest);

                            const candiateToSend: ServerToClientAuth.I_ICE_CANDIDATE = {
                                ice_candidate: data.ice_candidate
                            }
                            
                            this.send_to_client({type: "I_ICE_Candidate", data: candiateToSend}, userBConn );

                            break;
                        }

                        case "Offer": {
                            //comes from user A
                            const offer = requestAuth.data as ClientToServerAuth.I_OFFER;
    
 

                            const userA = offer.source;

                            const userB = offer.dest;
                            const userBSocket = this.userManager.getUser(userB);
                            
                            this.userManager.addConnectedUser(userA, userB);

                            const offer_to_user_b: ServerToClientAuth.IOffer = {offer: offer.offer, source: userA, dest: userB}


                            this.send_to_client({type: "OFFER", data: offer_to_user_b} , userBSocket);
                            break;
                        }
                        case "Answer": {
                            //comes from user B
                            const answer = requestAuth.data as ClientToServerAuth.I_Answer;

                            const userA = answer.source;
                            const userAConn = this.userManager.getUser(userA);

        
                            const userB =  answer.dest

                            //save call
                            this.userManager.addConnectedUser(userB, userA);

                            const answer_to_user_a: ServerToClientAuth.IAnswer = {answer: answer.answer}
                    
                            this.send_to_client({type: "ANSWER", data: answer_to_user_a}, userAConn);



                            break;
                        }

                    }
               
            }

            client_socket.onclose = (event: WebSocket.CloseEvent) => {
                console.log(`CLOSE`);
                this.userManager.removeUserBySocket(event.target);
                printClients();
            }

            const printClients = () => {
                let count = 0;
                let listUsers: string = "";
                for (let entry of this.userManager.getUsers()) {
                    count++;
                    listUsers += `_${entry[0].value}`;
                    console.log(`user connected: ${entry[0].value}`);
                }

                for (let entry of this.userManager.getUsers()) {
                    const data_to_send: ServerToClientAuth.IUserOnline = {user_string_formatted: listUsers};
                    
                    this.send_to_client({type: "IUserOnline", data: data_to_send},  entry[1]);
                }


                console.log(`there are ${count} count this.userManager`);
            }
            
        });
    
    }

    send_to_client = (data: ServerToClientAuth.DTO, socket: WebSocket) => {
        const data_to_send = JSON.stringify(data);
        console.log(`SENDINGSENDING ${data_to_send}`);
        socket.send(data_to_send);
    }





}


new DewiServer3();
